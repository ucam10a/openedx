# access bash
sudo docker exec -it <container_id> bash

# start container
sudo docker run -itd -p inbound_port:outbound_port <image>

# stop container
sudo docker stop <container_id>

# show all container
docker ps

# show last container
docker ps –l

# stop all container
docker stop $(docker ps -a -q)

# remove all container
docker rm $(docker ps -a -q)